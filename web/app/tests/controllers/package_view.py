# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2021 Baptiste Beauplat <lyknode@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from datetime import datetime
from logging import getLogger
from re import search, escape, sub
from urllib.parse import quote_plus

log = getLogger(__name__)


class PackageViewController():
    def __init__(self, snapshot):
        self.snapshot = snapshot

    def has_packages(self, category, haystack):
        packages = self.snapshot.get_source_packages_startswith(category)
        needle = fr'<h1>(Source|Binary) packages {category}\*</h1>\s+<p>\s+'

        for package in packages:
            needle += fr'<a href="{package}/">{package}</a><br />\s+'

        needle += r'</p>\s+'

        return search(needle, haystack)

    def has_source_versions(self, package, haystack):
        versions = self.snapshot.get_source_versions(package)
        needle = fr'<h1>Source package {escape(package)}</h1>\s+'
        needle += r'<p>\s+Available versions:\s+</p>\s+<ul>\s+'

        for version in versions:
            needle += fr'<li><a href="{version}/">{version}</a></li>\s+'

        needle += r'</ul>\s+'

        return search(needle, haystack)

    def has_binary_versions(self, package, haystack):
        versions = self.snapshot.get_binary_versions(package)
        needle = fr'<h1>Binary package {escape(package)}</h1>\s+'
        needle += r'<p>\s+Available versions:\s+</p>\s+<ul>\s+'

        for binary, binary_version, source, version in \
                [version.values() for version in versions]:
            needle += fr'<li><a href="../../package/' \
                      fr'{escape(quote_plus(source))}/{version}' \
                      fr'/#{self._escape_binary_link(binary)}' \
                      fr'_{binary_version}">{binary_version} ' \
                      fr'\(source: {escape(source)}' \
                      fr' {version}\)</a></li>\s+'

        needle += r'</ul>\s+'

        return search(needle, haystack)

    def _has_info_block(self, files):
        needle = ''

        for digest in files:
            needle += r'<dt style="font-size: x-small"><code>' \
                      fr'{digest}</code>:</dt>\s+<dd>\s+<dl>\s+'

            for name, size, first_seen, archive_name, path in \
                    [info.values() for info in
                     self.snapshot.get_file_info(digest)]:
                date = datetime.strptime(first_seen, '%Y%m%dT%H%M%SZ')
                needle += fr'<dt><a href="/archive/{archive_name}/' \
                          fr'{first_seen}{escape(quote_plus(path, safe="/"))}' \
                          fr'/{escape(quote_plus(name))}">' \
                          fr'<code style="font-size: (x-|)large"><strong>' \
                          fr'{escape(name)}</strong></code></a></dt>\s+' \
                          fr'<dd>\s+Seen in {archive_name} on ' \
                          fr'{date.isoformat(" ")} in\s+' \
                          fr'<a href="/archive/{archive_name}/{first_seen}' \
                          fr'{escape(quote_plus(path, safe="/"))}/">' \
                          fr'{escape(path)}</a>\.\s+<br />\s+Size: {size}\s+' \
                          r'</dd>\s+'

            needle += r'</dl>\s+</dd>\s+'

        return needle

    def _escape_binary_link(self, package):
        return sub(r'%(..)', lambda matches: f':{matches.group(1).lower()}:',
                   escape(quote_plus(package)))

    def has_package_version(self, package, version, haystack):
        source_files = self.snapshot.get_source_files(package, version)
        binary_packages = self.snapshot.get_binary_packages(package, version)
        needle = fr'<h1>Source package {escape(package)} {version}</h1>\s+' \
                 r'<h3>Source files</h3>\s+<dl>\s+'

        needle += self._has_info_block(source_files)

        needle += r'</dl>\s+' \
                  r'<h2><a name="binpkgs">Binary packages</a></h2>\s+' \
                  r'<ul>\s+'

        for binary_package, binary_version in [entry.values() for entry
                                               in binary_packages]:
            needle += fr'<li><a href="' \
                      fr'#{self._escape_binary_link(binary_package)}' \
                      fr'_{binary_version}">' \
                      fr'{escape(binary_package)} {binary_version}</a></li>\s+'

        needle += r'</ul>\s+'

        for binary_package, binary_version in [entry.values() for entry
                                               in binary_packages]:
            needle += fr'<h3><a name=' \
                      fr'"{self._escape_binary_link(binary_package)}' \
                      fr'_{binary_version}">' \
                      fr'{escape(binary_package)}' \
                      fr' {binary_version}</a></h3>\s+' \
                      r'<div style="font-size: x-small">' \
                      r'<a href="#top">top</a>' \
                      r' - <a href="#binpkgs">' \
                      r'up to binary packages</a></div>\s+<dl>\s+'

            binary_files = self.snapshot.get_binary_files(
                binary_package, binary_version
            )

            needle += self._has_info_block([info['hash'] for info
                                            in binary_files])

        needle += r'</dl>\s+'

        return search(needle, haystack)
