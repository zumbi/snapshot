# snapshot.debian.org - web frontend
#
# Copyright (c) 2009, 2010 Peter Palfrader
# Copyright (c) 2021 Baptiste Beauplat <lyknode@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from flask import Blueprint, make_response, current_app, abort

from snapshot.models.snapshot import get_snapshot_model
from snapshot.lib.cache import cache

router = Blueprint("project", __name__, url_prefix="/project")


@router.route('/trace/<string:domain>')
@cache()
def project_trace(domain):
    project_trace.cache_timeout = current_app.config['CACHE_TIMEOUT_ROOT']

    if domain != current_app.config.get("MASTER_DOMAIN"):
        abort(404)

    last = get_snapshot_model().get_last_mirrorrun()
    response = make_response(
        f'{last.ctime()}\n'
        '# Above timestamp is timestamp of latest mirrrorrun\n'
    )
    response.mimetype = 'text/plain'

    return response
