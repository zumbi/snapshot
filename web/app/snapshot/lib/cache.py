# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from datetime import datetime, timedelta
from functools import wraps

from flask import make_response


def cache():
    def decorator(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            rv = f(*args, **kwargs)

            if rv and 'Cache-Control' in getattr(rv, 'headers', {}):
                return rv

            response = make_response(rv)

            expires = datetime.utcnow() + \
                timedelta(seconds=wrapped.cache_timeout)

            response.cache_control.max_age = wrapped.cache_timeout
            response.cache_control.public = True
            response.headers.add('Expires',
                                 expires.strftime('%a, %d %b %Y %H:%M:%S GMT'))

            return response

        return wrapped

    return decorator
